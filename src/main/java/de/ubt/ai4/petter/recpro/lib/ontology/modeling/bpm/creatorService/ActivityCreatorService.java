package de.ubt.ai4.petter.recpro.lib.ontology.modeling.bpm.creatorService;

import de.ubt.ai4.petter.recpro.lib.bpm.model.modeling.Activity;
import de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyBaseIds;
import lombok.AllArgsConstructor;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.*;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyIds.OBJECT_PROPERTY_ACTIVITY_HAS_ROLE;

@Service
@AllArgsConstructor
public class ActivityCreatorService {
    public static Activity activityFromResource(Resource resource, OntModel model) {
        Activity result = new Activity();
        ElementCreatorService.initialize(result, resource, model);
        result.setRoles(RoleCreatorService.rolesFromResource(resource, model, OBJECT_PROPERTY_ACTIVITY_HAS_ROLE));
        return result;
    }

    public static Activity fromResource(Resource resource, OntModel model) {
        Activity result = new Activity();
        ElementCreatorService.initialize(result, resource, model);
        result.setRoles(RoleCreatorService.rolesFromResource(resource, model, OBJECT_PROPERTY_ACTIVITY_HAS_ROLE));
        return result;
    }

    public static List<Activity> activitiesFromResIterator(ResIterator instances, OntModel model) {
        List<Activity> result = new ArrayList<>();
        while (instances.hasNext()) {
            Resource activityInstance = instances.nextResource();
            result.add(activityFromResource(activityInstance, model));
        }
        return result;
    }

    public static List<Activity> activitiesFromExtendedIterator(ExtendedIterator<Resource> extendedIterator, OntModel model) {
        List<Activity> result = new ArrayList<>();
        while (extendedIterator.hasNext()) {
            Resource activityInstance = extendedIterator.next();
            result.add(activityFromResource(activityInstance, model));
        }
        return result;
    }

    public static List<Activity> activitiesFromResource(Resource resource, OntModel model, String uri) {
        Property property = model.getProperty(OntologyBaseIds.BASE_URL + uri);
        StmtIterator iterator = model.listStatements(resource, property, (RDFNode) null);

        List<Activity> result = new ArrayList<>();

        while (iterator.hasNext()) {
            Statement stmt = iterator.nextStatement();
            Resource activity = stmt.getObject().asResource();
            result.add(activityFromResource(activity, model));
        }

        return result;
    }
}
