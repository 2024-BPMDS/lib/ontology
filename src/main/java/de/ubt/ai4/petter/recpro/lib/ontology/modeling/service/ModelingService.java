package de.ubt.ai4.petter.recpro.lib.ontology.modeling.service;

import lombok.AllArgsConstructor;
import org.apache.jena.ontology.*;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.vocabulary.RDF;
import org.springframework.stereotype.Service;

import static de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyBaseIds.BASE_URL;

@Service
@AllArgsConstructor
public class ModelingService {
    public static Statement getPropertyFromResource(Resource resource, OntModel model, String URI) {
        return resource.getProperty(getPropertyFromModel(model, URI));
    }

    public static Property getPropertyFromModel(OntModel model, String URI) {
        return model.getProperty(BASE_URL + URI);
    }

    public static ObjectProperty getObjectPropertyFromModel(OntModel model, String URI) {
        return model.getObjectProperty(BASE_URL + URI);
    }

    public static Resource getResourceFromModel(OntModel model, String URI) {
        return model.getResource(BASE_URL + URI);
    }

    public static String getStringPropertyFromResource(Resource resource, OntModel model, String URI) {
        Statement statement = getPropertyFromResource(resource, model, URI);
        if (statement != null) {
            return statement.getString();
        } else {
            return "";
        }
    }

    public static OntClass getOntClass(OntModel model, String classType) {
        return model.getOntClass(BASE_URL + classType);
    }

    public static Individual getIndividual(OntModel model, String path, OntClass ontClass) {
        return model.createIndividual(BASE_URL + path, ontClass);
    }

    public static Individual getIndividual(OntModel model, String classPath, String instancePath) {
        OntClass ontClass = getOntClass(model, classPath);
        return getIndividual(model, instancePath, ontClass);
    }

    public static Individual addDatatypeProperty(OntModel model, String dataTypePropertyPath, Individual individual, String content) {
        if (content != null) {
            DatatypeProperty property = getDatatypeProperty(model, dataTypePropertyPath);
            if (individual.hasProperty(property)) {
                individual.removeAll(property);
            }
            individual.addProperty(property, content);
        }
        return individual;
    }

    public static DatatypeProperty getDatatypeProperty(OntModel model, String path) {
        return model.getDatatypeProperty(BASE_URL + path);
    }

    public static ResIterator getResIterator(OntModel model, String resourcePath) {
        Resource resource = getResourceFromModel(model, resourcePath);
        return model.listResourcesWithProperty(RDF.type, resource);
    }

    public static void addProperty(Individual subject, ObjectProperty predicate, Individual object) {
        subject.addProperty(predicate, object);
    }

    public static void setProperty(Individual subject, ObjectProperty predicate, Individual object) {
        subject.setPropertyValue(predicate, object);
    }

    public static void delete(OntModel model, String type, String id) {
        Individual individual = ModelingService.getIndividual(model, type, id);
        model.removeAll(individual, null, null);
        model.removeAll(null, null, individual);
        individual.remove();
    }
}
