package de.ubt.ai4.petter.recpro.lib.ontology.modeling.user;

import de.ubt.ai4.petter.recpro.lib.bpm.model.modeling.User;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.service.ModelService;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.service.ModelingService;
import lombok.RequiredArgsConstructor;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.ObjectProperty;
import org.apache.jena.ontology.OntModel;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

import static de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyIds.*;
import static de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyUserIds.*;

@Service
@RequiredArgsConstructor
public class UserModelingService {

    private final ModelService modelService;

    public void createUser(User user) {
        if (user.getId().isEmpty()) {
            user.setId(UUID.randomUUID().toString());
        }

        OntModel model = modelService.getModel();

        Individual userIndividual = ModelingService.getIndividual(model, USER, user.getId());

        createDataProperties(userIndividual, model, user);

        user.getRoles().forEach(role -> {
            Individual roleIndividual = ModelingService.getIndividual(model, ROLE, role.getId());
            createObjectProperties(model, userIndividual, roleIndividual);
        });

        modelService.writeModel(model);
    }

    private void createDataProperties(Individual individual, OntModel model, User user) {
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_USER_HAS_ID, individual, user.getId());
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_USER_HAS_DESCRIPTION, individual, user.getDescription());
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_USER_HAS_FIRST_NAME, individual, user.getFirstName());
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_USER_HAS_LAST_NAME, individual, user.getLastName());
    }

    private void createObjectProperties(OntModel model, Individual userIndividual, Individual roleIndividual) {
        ObjectProperty userHasRole = ModelingService.getObjectPropertyFromModel(model, OBJECT_PROPERTY_USER_HAS_ROLE);

        ModelingService.setProperty(userIndividual, userHasRole, roleIndividual);
    }

    public User getById(String userId) {
        OntModel model = modelService.getModel();
        Individual userIndividual = ModelingService.getIndividual(model, USER, userId);
        return UserCreatorService.fromResource(userIndividual, model);
    }

    public List<User> getAll() {
        OntModel model = modelService.getModel();
        return UserCreatorService.fromIterator(ModelingService.getResIterator(model, USER), model);
    }

    public void delete(String userId) {
        OntModel model = modelService.getModel();
        Individual ratingIndividual = ModelingService.getIndividual(model, USER, userId);
        model.removeAll(ratingIndividual, null, null);
        model.removeAll(null, null, ratingIndividual);
        ratingIndividual.remove();
        modelService.writeModel(model);
    }
}
