package de.ubt.ai4.petter.recpro.lib.ontology.modeling.filter.base;

import de.ubt.ai4.petter.recpro.lib.filter.basefilter.model.Filter;
import de.ubt.ai4.petter.recpro.lib.filter.basefilter.model.FilterType;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/modeling/filter/")
@AllArgsConstructor
public class FilterModelingController {

    private FilterModelingService modelingService;
    @PostMapping("create")
    public ResponseEntity<Filter> create(@RequestBody Filter filter) {
        return ResponseEntity.ok(modelingService.create(filter));
    }

    @GetMapping("getAll")
    public ResponseEntity<List<Filter>> getAll() {
        return ResponseEntity.ok(modelingService.getAll());
    }

    @GetMapping("getById/{filterId}")
    public ResponseEntity<Filter> getById(@PathVariable String filterId) {
        return ResponseEntity.ok(modelingService.getById(filterId));
    }

    @DeleteMapping("delete/{filterId}")
    public void delete(@PathVariable String filterId) {
        this.modelingService.removeFilter(filterId);
    }

    @GetMapping("getByType/{filterType}")
    public ResponseEntity<List<Filter>> getByType(@PathVariable FilterType filterType) {
        return ResponseEntity.ok(this.modelingService.getByType(filterType));
    }
}
