package de.ubt.ai4.petter.recpro.lib.ontology.modeling.rating;

import de.ubt.ai4.petter.recpro.lib.ontology.modeling.service.ModelingService;
import de.ubt.ai4.petter.recpro.lib.rating.rating.modeling.model.*;
import lombok.AllArgsConstructor;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyIds.*;
import static de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyBaseIds.*;

@Service
@AllArgsConstructor

public class RatingCreatorService {

    public static List<Rating> fromIterator(ResIterator iterator, OntModel model) {
        List<Rating> result = new ArrayList<>();

        while(iterator.hasNext()) {
            Resource ratingInstance = iterator.nextResource();
            result.add(fromResource(ratingInstance, model));
        }

        return result;
    }

    public static Rating fromResource(Resource resource, OntModel model) {
        Rating result = new Rating();
        initializeRating(result, resource, model);

        return switch (result.getRatingType()) {
            case ORDINAL -> fromOrdinalResource(resource, model);
            case BINARY -> fromBinaryResource(resource, model);
            case CONTINUOUS -> fromContinuousResource(resource, model);
            case UNARY -> fromUnaryResource(resource, model);
            case INTERVAL_BASED -> fromIntervalBasedResource(resource, model);
        };
    }

    public static List<Rating> getListFromResource(Resource resource, OntModel model, String uri) {
        Property property = model.getProperty(BASE_URL + uri);
        StmtIterator iterator = model.listStatements(resource, property, (RDFNode) null);
        List<Rating> result = new ArrayList<>();
        while (iterator.hasNext()) {
            Statement stmt = iterator.nextStatement();
            Resource element = stmt.getObject().asResource();
            Rating rating = fromResource(element, model);
            if (!rating.getId().isEmpty() && !rating.getId().isBlank()) {
                result.add(fromResource(element, model));
            }
        }
        return result;
    }

    private static Rating fromBinaryResource(Resource resource, OntModel model) {
        BinaryRating result = new BinaryRating();
        initializeRating(result, resource, model);
        result.setDefaultValue(Boolean.parseBoolean(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_RATING_BINARY_RATING_DEFAULT_VALUE)));
        result.setTrueLabel(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_RATING_BINARY_RATING_TRUE_LABEL));
        result.setFalseLabel(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_RATING_BINARY_RATING_FALSE_LABEL));
        return result;
    }

    private static Rating fromUnaryResource(Resource resource, OntModel model) {
        UnaryRating result = new UnaryRating();
        initializeRating(result, resource, model);
        result.setDefaultValue(Boolean.parseBoolean(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_RATING_UNARY_RATING_DEFAULT_VALUE)));
        result.setLabel(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_RATING_UNARY_RATING_LABEL));
        return result;
    }

    private static Rating fromContinuousResource(Resource resource, OntModel model) {
        ContinuousRating result = new ContinuousRating();
        initializeRating(result, resource, model);
        result.setDefaultValue(Long.valueOf(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_RATING_CONTINUOUS_RATING_DEFAULT_VALUE)));
        result.setScale(RatingScaleCreatorService.fromResource(resource, model));
        return result;
    }

    private static Rating fromIntervalBasedResource(Resource resource, OntModel model) {
        IntervalBasedRating result = new IntervalBasedRating();
        initializeRating(result, resource, model);
        result.setDefaultValue(Integer.valueOf(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_RATING_INTERVAL_BASED_RATING_DEFAULT_VALUE)));
        result.setScale(RatingScaleCreatorService.fromResource(resource, model));
        return result;
    }

    private static Rating fromOrdinalResource(Resource resource, OntModel model) {
        OrdinalRating result = new OrdinalRating();
        initializeRating(result, resource, model);
        result.setDefaultValue(Integer.valueOf(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_RATING_ORDINAL_RATING_DEFAULT_VALUE)));
        result.setScale(RatingScaleCreatorService.fromResource(resource, model));
        return result;
    }

    private static void initializeRating(Rating rating, Resource resource, OntModel model) {
        rating.setId(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_RATING_ID));
        rating.setName(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_RATING_NAME));
        rating.setDescription(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_RATING_DESCRIPTION));

        String ratingType = ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_RATING_TYPE);

        if (!ratingType.isEmpty()) {
            rating.setRatingType(RatingType.valueOf(ratingType));
        } else {
            rating.setRatingType(null);
        }

    }
}
