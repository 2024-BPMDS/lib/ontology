package de.ubt.ai4.petter.recpro.lib.ontology.modeling.rating;

import de.ubt.ai4.petter.recpro.lib.ontology.modeling.service.ModelService;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.service.ModelingService;
import de.ubt.ai4.petter.recpro.lib.rating.rating.modeling.model.*;
import lombok.RequiredArgsConstructor;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.ObjectProperty;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.UUID;

import static de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyIds.*;

@Service
@RequiredArgsConstructor
public class RatingModelingService {

    private final ModelService modelService;
    private final RatingScaleModelingService ratingScaleModelingService;

    @Value("${recpro.rating.default}")
    private String defaultRatingId;

    public void setDefaultRating(String ratingId) {
        this.delete(this.defaultRatingId);
        Rating rating = this.getRatingById(ratingId);
        rating.setId(this.defaultRatingId);
        this.createRating(rating);
    }

    public void delete(String ratingId) {
        OntModel model = modelService.getModel();
        Individual ratingIndividual = ModelingService.getIndividual(model, RATING, ratingId);
        model.removeAll(ratingIndividual, null, null);
        model.removeAll(null, null, ratingIndividual);
        ratingIndividual.remove();
        modelService.writeModel(model);

    }

    public List<Rating> getAll() {
        OntModel model = modelService.getModel();
        return RatingCreatorService.fromIterator(ModelingService.getResIterator(model, RATING), model);
    }

    public Rating getByBpmElementId(String bpmElementId) {
        OntModel model = modelService.getModel();

        Resource bpmElement = ModelingService.getResourceFromModel(model, bpmElementId);
        Property hasRating = ModelingService.getObjectPropertyFromModel(model, OBJECT_PROPERTY_BPM_ELEMENT_HAS_RATING);

        StmtIterator iterator = model.listStatements(bpmElement, hasRating, (RDFNode) null);

        if (iterator.hasNext()) {
            Statement statement = iterator.nextStatement();
            Resource resource = statement.getObject().asResource();

            return RatingCreatorService.fromResource(resource, model);
        } else {
            return getDefaultRating();
        }
    }

    public Rating getDefaultRating() {
        return getRatingById(this.defaultRatingId);
    }

    public Rating getRatingById(String ratingId) {
        OntModel model = modelService.getModel();
        Individual ratingIndividual = ModelingService.getIndividual(model, RATING, ratingId);
        return RatingCreatorService.fromResource(ratingIndividual, model);
    }

    public void createRating(Rating rating) {
        if (rating.getId().isEmpty()) {
            rating.setId(UUID.randomUUID().toString());
        }

        OntModel model = modelService.getModel();
        Individual ratingIndividual;

        switch (rating.getRatingType()) {
            case BINARY -> {
                ratingIndividual = ModelingService.getIndividual(model, RATING_BINARY_RATING, rating.getId());
                createBinaryDataProperties(ratingIndividual, model, rating);
            }
            case UNARY ->  {
                ratingIndividual = ModelingService.getIndividual(model, RATING_UNARY_RATING, rating.getId());
                createUnaryDataProperties(ratingIndividual, model,rating);
            }
            case ORDINAL -> {
                ratingIndividual = ModelingService.getIndividual(model, RATING_ORDINAL_RATING, rating.getId());
                this.createOrdinalDataProperties(ratingIndividual, model, rating);
                model = ratingScaleModelingService.createRatingScale(((OrdinalRating) rating).getScale(), ratingIndividual, model);
            }
            case CONTINUOUS -> {
                ratingIndividual = ModelingService.getIndividual(model, RATING_CONTINUOUS_RATING, rating.getId());
                this.createContinuousDataProperties(ratingIndividual, model, rating);
                model = ratingScaleModelingService.createRatingScale(((ContinuousRating) rating).getScale(), ratingIndividual, model);
            }
            case INTERVAL_BASED -> {
                ratingIndividual = ModelingService.getIndividual(model, RATING_INTERVAL_BASED_RATING, rating.getId());
                this.createIntervalBasedDataProperties(ratingIndividual, model, rating);
                model = ratingScaleModelingService.createRatingScale(((IntervalBasedRating) rating).getScale(), ratingIndividual, model);
            }
            default -> ratingIndividual = ModelingService.getIndividual(model, RATING, rating.getId());
        }

        this.createDataProperties(ratingIndividual, model, rating);

        modelService.writeModel(model);
    }

    public void addRatingToBpmElement(String ratingId, String elementId) {
        OntModel model = modelService.getModel();
        Individual elementIndividual = ModelingService.getIndividual(model, RECPRO_ELEMENT, elementId);
        Individual ratingIndividual = ModelingService.getIndividual(model, RATING, ratingId);

        this.createObjectProperties(model, elementIndividual, ratingIndividual);
    }

    private void createDataProperties(Individual individual, OntModel model, Rating rating) {
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_RATING_ID, individual, rating.getId());
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_RATING_NAME, individual, rating.getName());
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_RATING_DESCRIPTION, individual, rating.getDescription());
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_RATING_TYPE, individual, String.valueOf(rating.getRatingType()));
    }

    private void createBinaryDataProperties(Individual individual, OntModel model, Rating rating) {
        BinaryRating binaryRating = (BinaryRating) rating;
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_RATING_BINARY_RATING_DEFAULT_VALUE, individual, String.valueOf(binaryRating.isDefaultValue()));
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_RATING_BINARY_RATING_FALSE_LABEL, individual, binaryRating.getFalseLabel());
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_RATING_BINARY_RATING_TRUE_LABEL, individual, binaryRating.getTrueLabel());
    }

    private void createContinuousDataProperties(Individual individual, OntModel model, Rating rating) {
        ContinuousRating continuousRating = (ContinuousRating) rating;
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_RATING_CONTINUOUS_RATING_DEFAULT_VALUE, individual, String.valueOf(continuousRating.getDefaultValue()));
    }

    private void createIntervalBasedDataProperties(Individual individual, OntModel model, Rating rating) {
        IntervalBasedRating intervalBasedRating = (IntervalBasedRating) rating;
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_RATING_INTERVAL_BASED_RATING_DEFAULT_VALUE, individual, String.valueOf(intervalBasedRating.getDefaultValue()));
    }

    private void createOrdinalDataProperties(Individual individual, OntModel model, Rating rating) {
        OrdinalRating ordinalRating = (OrdinalRating) rating;
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_RATING_ORDINAL_RATING_DEFAULT_VALUE, individual, String.valueOf(ordinalRating.getDefaultValue()));
    }

    private void createUnaryDataProperties(Individual individual, OntModel model, Rating rating) {
        UnaryRating unaryRating = (UnaryRating) rating;
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_RATING_UNARY_RATING_DEFAULT_VALUE, individual, String.valueOf(unaryRating.isDefaultValue()));
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_RATING_UNARY_RATING_LABEL, individual, unaryRating.getLabel());
    }

    private void createObjectProperties(OntModel model, Individual elementIndividual, Individual ratingIndividual) {
        ObjectProperty bpmElementHasRating = ModelingService.getObjectPropertyFromModel(model, OBJECT_PROPERTY_BPM_ELEMENT_HAS_RATING);

        ModelingService.setProperty(elementIndividual, bpmElementHasRating, ratingIndividual);
    }

}
