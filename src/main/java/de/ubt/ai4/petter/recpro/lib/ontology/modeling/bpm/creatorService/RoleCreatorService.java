package de.ubt.ai4.petter.recpro.lib.ontology.modeling.bpm.creatorService;

import de.ubt.ai4.petter.recpro.lib.bpm.model.modeling.BpmElement;
import de.ubt.ai4.petter.recpro.lib.bpm.model.modeling.Role;
import de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyBaseIds;
import de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyIds;
import lombok.AllArgsConstructor;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static de.ubt.ai4.petter.recpro.lib.ontology.modeling.bpm.creatorService.ElementCreatorService.fromResource;

@Service
@AllArgsConstructor
public class RoleCreatorService {

    public static List<BpmElement> fromResIterator(ResIterator individuals, OntModel model) {
        List<BpmElement> result = new ArrayList<>();
        while (individuals.hasNext()) {
            Resource roleInstance = individuals.nextResource();
            result.add(fromResource(roleInstance, model));
        }
        return result;
    }

    public static Role roleFromResource(Resource resource, OntModel model) {
        Role result = new Role();
        ElementCreatorService.initialize(result, resource, model);
        result.setChildElements(rolesFromResource(resource, model, OntologyIds.OBJECT_PROPERTY_ROLE_HAS_CHILD));
        return result;
    }

    public static List<Role> rolesFromResource(Resource resource, OntModel model, String uri) {
        Property processModelHasActivity = model.getProperty(OntologyBaseIds.BASE_URL + uri);
        StmtIterator iterator = model.listStatements(resource, processModelHasActivity, (RDFNode) null);

        List<Role> result = new ArrayList<>();

        while (iterator.hasNext()) {
            Statement stmt = iterator.nextStatement();
            Resource role = stmt.getObject().asResource();
            result.add(roleFromResource(role, model));
        }

        return result;
    }
}
