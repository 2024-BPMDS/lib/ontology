package de.ubt.ai4.petter.recpro.lib.ontology.modeling.rating;

import de.ubt.ai4.petter.recpro.lib.ontology.modeling.service.ModelingService;
import de.ubt.ai4.petter.recpro.lib.rating.rating.modeling.model.ScaleDetail;
import lombok.AllArgsConstructor;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyIds.*;
import static de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyBaseIds.*;

@Service
@AllArgsConstructor
public class RatingScaleDetailCreatorService {

    public static List<ScaleDetail> scaleDetailsFromResource(Resource resource, OntModel model) {
        Property property = model.getProperty(BASE_URL + OBJECT_PROPERTY_RATING_SCALE_HAS_SCALE_DETAIL);
        StmtIterator iterator = model.listStatements(resource, property, (RDFNode) null);

        return fromIterator(iterator, model);
    }

    public static List<ScaleDetail> fromIterator(StmtIterator iterator, OntModel model) {
        List<ScaleDetail> result = new ArrayList<>();
        while(iterator.hasNext()) {
            Statement stmt = iterator.nextStatement();
            Resource scaleDetail = stmt.getObject().asResource();
            result.add(fromResource(scaleDetail, model));
        }
        return result;
    }

    public static ScaleDetail fromResource(Resource resource, OntModel model) {
        ScaleDetail scaleDetail = new ScaleDetail();
        scaleDetail.setId(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_RATING_SCALE_DETAIL_ID));
        scaleDetail.setLabel(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_RATING_SCALE_DETAIL_LABEL));
        scaleDetail.setValue(Long.valueOf(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_RATING_SCALE_DETAIL_VALUE)));
        scaleDetail.setPosition(Integer.valueOf(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_RATING_SCALE_DETAIL_POSITION)));
        return scaleDetail;
    }

}
