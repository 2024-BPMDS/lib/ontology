package de.ubt.ai4.petter.recpro.lib.ontology.modeling.bpm.creatorService;

import de.ubt.ai4.petter.recpro.lib.bpm.model.modeling.BpmElement;
import de.ubt.ai4.petter.recpro.lib.bpm.model.modeling.ElementType;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.attribute.AttributeCreatorService;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.service.ModelingService;
import de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyIds;
import lombok.AllArgsConstructor;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyBaseIds.BASE_URL;

@Service
@AllArgsConstructor
public class ElementCreatorService {

    public static List<BpmElement> fromResIterator(ResIterator instances, OntModel model) {
        List<BpmElement> result = new ArrayList<>();
        while (instances.hasNext()) {
            Resource roleInstance = instances.nextResource();
            BpmElement recproElement = fromResource(roleInstance, model);
            if (!recproElement.getId().isEmpty() && !recproElement.getId().isBlank()) {
                result.add(recproElement);
            }
        }
        return result;
    }

    public static BpmElement fromResource(Resource resource, OntModel model) {
        BpmElement res = new BpmElement();
        initialize(res, resource, model);
        return res;
    }

    public static List<BpmElement> getListFromResource(Resource resource, OntModel model, String uri) {
        Property property = model.getProperty(BASE_URL + uri);
        StmtIterator iterator = model.listStatements(resource, property, (RDFNode) null);
        List<BpmElement> result = new ArrayList<>();
        while (iterator.hasNext()) {
            Statement stmt = iterator.nextStatement();
            Resource element = stmt.getObject().asResource();
            BpmElement recproElement = fromResource(element, model);
            if (!recproElement.getId().isEmpty() && !recproElement.getId().isBlank()) {
                result.add(fromResource(element, model));
            }
        }
        return result;
    }

    public static void initialize(BpmElement element, Resource resource, OntModel model) {
        element.setId(ModelingService.getStringPropertyFromResource(resource, model, OntologyIds.DATA_PROPERTY_RECPRO_ELEMENT_ID));
        element.setName(ModelingService.getStringPropertyFromResource(resource, model, OntologyIds.DATA_PROPERTY_RECPRO_ELEMENT_NAME));
        element.setDescription(ModelingService.getStringPropertyFromResource(resource, model, OntologyIds.DATA_PROPERTY_RECPRO_ELEMENT_DESCRIPTION));
        element.setAttributes(AttributeCreatorService.getListFromResource(resource, model, OntologyIds.OBJECT_PROPERTY_RECPRO_ELEMENT_HAS_ATTRIBUTE));
        element.setElementType(ElementType.findType(ModelingService.getStringPropertyFromResource(resource, model, OntologyIds.DATA_PROPERTY_RECPRO_ELEMENT_TYPE)));
    }
}
