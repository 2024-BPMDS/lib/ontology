package de.ubt.ai4.petter.recpro.lib.ontology.modeling.service;

import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.RDFDataMgr;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

@Service
public class ModelService {
    @Value("${recpro.ontology.model.path}")
    private String modelPath;

    @Value("${recpro.ontology.model.default.path}")
    private  String defaultModelPath;

    public OntModel getModel() {
        return getOntologyModelFromPath(modelPath);
    }

    private OntModel getOntologyModelFromPath(String path) {
        Model model = ModelFactory.createDefaultModel();

        InputStream in = RDFDataMgr.open(path);



        if (in != null) {
            model.read(in, null);
            OntModel ontModel = ModelFactory.createOntologyModel();
            ontModel.add(model);
            return ontModel;
        } else {
            throw new IllegalArgumentException("FILE: " + path + " not found");
        }

    }

    public void writeModel(OntModel model) {
        writeModelToPath(model, modelPath);
    }

    private void writeModelToPath(OntModel model, String path) {
        try (OutputStream outputStream = new FileOutputStream(path)) {
            model.write(outputStream, "RDF/XML");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearOntology() {
        writeModelToPath(createEmptyOntology(), modelPath);
    }

    private OntModel createEmptyOntology() {
        return getOntologyModelFromPath(defaultModelPath);
    }
}
