package de.ubt.ai4.petter.recpro.lib.ontology;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"de.ubt.ai4.petter.recpro", "de.ubt.ai4.petter.recpro.lib.attribute", "de.ubt.ai4.petter.recpro.lib.bpm", "de.ubt.ai4.petter.recpro.lib.bpms"})
public class OntologyApplication {

	public static void main(String[] args) {
		SpringApplication.run(OntologyApplication.class, args);
	}

}
