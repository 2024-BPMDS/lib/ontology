package de.ubt.ai4.petter.recpro.lib.ontology.modeling.filter.base;

import de.ubt.ai4.petter.recpro.lib.filter.basefilter.model.BaseFilter;
import de.ubt.ai4.petter.recpro.lib.filter.basefilter.model.TasklistOrder;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.filter.util.FilterCreatorUtil;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.service.ModelingService;
import lombok.AllArgsConstructor;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.Resource;
import org.springframework.stereotype.Service;

import static de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyFilterIds.*;

@AllArgsConstructor
@Service
public class BaseFilterCreatorService {
    private FilterCreatorUtil filterCreatorUtil;

    public BaseFilter fromResource(Resource resource, OntModel model) {
        BaseFilter result = new BaseFilter();
        filterCreatorUtil.initialize(result, resource, model);
        result.setAscending(Boolean.parseBoolean(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_FILTER_TASKLIST_ASCENDING)));
        result.setTasklistOrder(TasklistOrder.valueOf(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_FILTER_TASKLIST_ORDER)));

        return result;
    }
}
