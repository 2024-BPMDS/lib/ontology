package de.ubt.ai4.petter.recpro.lib.ontology.modeling.bpm.modelingService;

import de.ubt.ai4.petter.recpro.lib.bpm.model.modeling.BpmElement;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.bpm.creatorService.ElementCreatorService;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.service.ModelService;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.service.ModelingService;
import lombok.AllArgsConstructor;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.springframework.stereotype.Service;

import java.util.List;

import static de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyIds.*;


@AllArgsConstructor
@Service
public class OntologyModelingBpmElementService {
    private ModelService modelService;
    public Individual createRecproElementIndividual(BpmElement element, Individual individual, OntModel model) {
        individual = ModelingService.addDatatypeProperty(model, DATA_PROPERTY_RECPRO_ELEMENT_ID, individual, element.getId());
        individual = ModelingService.addDatatypeProperty(model, DATA_PROPERTY_RECPRO_ELEMENT_NAME, individual, element.getName());
        individual = ModelingService.addDatatypeProperty(model, DATA_PROPERTY_RECPRO_ELEMENT_DESCRIPTION, individual, element.getDescription());
        individual = ModelingService.addDatatypeProperty(model, DATA_PROPERTY_RECPRO_ELEMENT_TYPE, individual, element.getElementType().name());

        ModelingService.getIndividual(model, RECPRO_ELEMENT, element.getId());

        return individual;
    }

    public List<BpmElement> getAll() {
        OntModel model = modelService.getModel();
        ResIterator individuals = ModelingService.getResIterator(model, RECPRO_ELEMENT);
        return ElementCreatorService.fromResIterator(individuals, model);
    }

    public BpmElement getById(String bpmElementId) {
        OntModel model = modelService.getModel();
        ResIterator individuals = ModelingService.getResIterator(model, RECPRO_ELEMENT);
        ExtendedIterator<Resource> extendedIterator = individuals.filterKeep(
                resource -> resource.hasProperty(ModelingService.getPropertyFromModel(model, DATA_PROPERTY_RECPRO_ELEMENT_ID))
                        && bpmElementId.equals(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_RECPRO_ELEMENT_ID))
        );
        if (extendedIterator.hasNext()) {
            return ElementCreatorService.fromResource(extendedIterator.next(), model);
        } else {
            return new BpmElement();
        }
    }
}
