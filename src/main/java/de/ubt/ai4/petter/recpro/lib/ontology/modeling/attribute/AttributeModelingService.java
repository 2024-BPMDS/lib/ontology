package de.ubt.ai4.petter.recpro.lib.ontology.modeling.attribute;

import de.ubt.ai4.petter.recpro.lib.attribute.modeling.model.RecproAttribute;
import de.ubt.ai4.petter.recpro.lib.attribute.modeling.model.RecproBinaryAttribute;
import de.ubt.ai4.petter.recpro.lib.attribute.modeling.model.RecproNumericAttribute;
import de.ubt.ai4.petter.recpro.lib.attribute.modeling.model.RecproTextualAttribute;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.service.ModelService;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.service.ModelingService;
import lombok.AllArgsConstructor;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.ObjectProperty;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

import static de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyAttributeIds.*;
import static de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyIds.*;

@Service
@AllArgsConstructor
public class AttributeModelingService {
    private ModelService modelService;

    public List<RecproAttribute> getAll() {
        OntModel model = modelService.getModel();
        return AttributeCreatorService.fromIterator(ModelingService.getResIterator(model, ATTRIBUTE), model);
    }

    public void createAttribute(RecproAttribute attribute, List<String> elementIds) {
        elementIds.forEach(elementId -> this.createAttribute(attribute, elementId));
    }

    public void create(RecproAttribute attribute) {
        OntModel model = modelService.getModel();

        if (attribute.getId().isEmpty()) {
            attribute.setId(UUID.randomUUID().toString());
        }

        Individual attributeIndividual = ModelingService.getIndividual(model, ATTRIBUTE, attribute.getId());

        switch (attribute.getAttributeType()) {
            case TEXT -> {
                attributeIndividual = ModelingService.getIndividual(model, ATTRIBUTE_TEXTUAL_ATTRIBUTE, attribute.getId());
                this.createTextualDataProperties(attributeIndividual, model, attribute);
            }
            case NUMERIC -> {
                attributeIndividual = ModelingService.getIndividual(model, ATTRIBUTE_NUMERICAL_ATTRIBUTE, attribute.getId());
                this.createNumericalDataProperties(attributeIndividual, model, attribute);
            }
            case OBJECT -> {
            }
            case BINARY -> {
                attributeIndividual = ModelingService.getIndividual(model, ATTRIBUTE_BINARY_ATTRIBUTE, attribute.getId());
                this.createBinaryDataProperties(attributeIndividual, model, attribute);
            }

            case META -> {
                attributeIndividual = ModelingService.getIndividual(model, ATTRIBUTE_META_ATTRIBUTE, attribute.getId());
            }
        }

        this.createDataProperties(attributeIndividual, model, attribute);
        modelService.writeModel(model);
    }

    public void delete(String attributeId) {
        OntModel model = modelService.getModel();
        ModelingService.delete(model, ATTRIBUTE, attributeId);
        modelService.writeModel(model);
    }

    public void createAttribute(RecproAttribute attribute, String elementId) {
        OntModel model = modelService.getModel();

        if (attribute.getId().isEmpty()) {
            attribute.setId(UUID.randomUUID().toString());
        }

        Individual attributeIndividual = ModelingService.getIndividual(model, ATTRIBUTE, attribute.getId());
        Individual elementIndividual = ModelingService.getIndividual(model, RECPRO_ELEMENT, elementId);

        switch (attribute.getAttributeType()) {
            case TEXT -> {
                attributeIndividual = ModelingService.getIndividual(model, ATTRIBUTE_TEXTUAL_ATTRIBUTE, attribute.getId());
                this.createTextualDataProperties(attributeIndividual, model, attribute);
            }
            case NUMERIC -> {
                attributeIndividual = ModelingService.getIndividual(model, ATTRIBUTE_NUMERICAL_ATTRIBUTE, attribute.getId());
                this.createNumericalDataProperties(attributeIndividual, model, attribute);
            }
            case OBJECT -> {
            }
            case BINARY -> {
                attributeIndividual = ModelingService.getIndividual(model, ATTRIBUTE_BINARY_ATTRIBUTE, attribute.getId());
                this.createBinaryDataProperties(attributeIndividual, model, attribute);
            }

            case META -> {
                attributeIndividual = ModelingService.getIndividual(model, ATTRIBUTE_META_ATTRIBUTE, attribute.getId());
            }
        }

        this.createDataProperties(attributeIndividual, model, attribute);
        this.createObjectProperties(model, elementIndividual, attributeIndividual);
        modelService.writeModel(model);
    }

    private void createDataProperties(Individual attributeIndividual, OntModel model, RecproAttribute attribute) {
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_ATTRIBUTE_ID, attributeIndividual, attribute.getId());
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_ATTRIBUTE_NAME, attributeIndividual, attribute.getName());
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_ATTRIBUTE_DESCRIPTION, attributeIndividual, attribute.getDescription());
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_ATTRIBUTE_TYPE, attributeIndividual, attribute.getAttributeType().name());
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_ATTRIBUTE_BPMS_ID, attributeIndividual, attribute.getBpmsAttributeId());
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_ATTRIBUTE_FROM_URL, attributeIndividual, String.valueOf(attribute.isFromUrl()));
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_ATTRIBUTE_URL, attributeIndividual, attribute.getUrl());
    }

    private void createBinaryDataProperties(Individual attributeIndividual, OntModel model, RecproAttribute attribute) {
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_ATTRIBUTE_BINARY_ATTRIBUTE_DEFAULT_VALUE, attributeIndividual, String.valueOf(((RecproBinaryAttribute) attribute).isDefaultValue()));
    }

    private void createTextualDataProperties(Individual attributeIndividual, OntModel model, RecproAttribute attribute) {
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_ATTRIBUTE_TEXTUAL_ATTRIBUTE_DEFAULT_VALUE, attributeIndividual, ((RecproTextualAttribute) attribute).getDefaultValue());
    }

    private void createNumericalDataProperties(Individual attributeIndividual, OntModel model, RecproAttribute attribute) {
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_ATTRIBUTE_NUMERICAL_ATTRIBUTE_DEFAULT_VALUE, attributeIndividual, String.valueOf(((RecproNumericAttribute) attribute).getDefaultValue()));
    }

    private void createObjectProperties(OntModel model, Individual elementIndividual, Individual attributeIndividual) {
        ObjectProperty bpmElementHasAttribute = ModelingService.getObjectPropertyFromModel(model, OBJECT_PROPERTY_RECPRO_ELEMENT_HAS_ATTRIBUTE);
        ObjectProperty attributeIsPartOfBpmElement = ModelingService.getObjectPropertyFromModel(model, OBJECT_PROPERTY_ATTRIBUTE_IS_PART_OF_RECPRO_ELEMENT);

        ModelingService.addProperty(elementIndividual, bpmElementHasAttribute, attributeIndividual);
        ModelingService.addProperty(attributeIndividual, attributeIsPartOfBpmElement, elementIndividual);
    }

    public List<RecproAttribute> getByIds(List<String> attributeIds) {
        OntModel model = modelService.getModel();
        ResIterator instances = ModelingService.getResIterator(model, ATTRIBUTE);

        ExtendedIterator<Resource> extendedIterator = instances.filterKeep(
                o -> o.hasProperty(ModelingService.getPropertyFromModel(model, DATA_PROPERTY_ATTRIBUTE_ID))
                        && attributeIds.contains(ModelingService.getStringPropertyFromResource(o, model, DATA_PROPERTY_ATTRIBUTE_ID))
        );

        return AttributeCreatorService.fromIterator(extendedIterator, model);
    }

    public RecproAttribute getById(String attributeId) {
        OntModel model = modelService.getModel();
        ResIterator instances = ModelingService.getResIterator(model, ATTRIBUTE);

        ExtendedIterator<Resource> extendedIterator = instances.filterKeep(
                o -> o.hasProperty(ModelingService.getPropertyFromModel(model, DATA_PROPERTY_ATTRIBUTE_ID))
                        && attributeId.equals(ModelingService.getStringPropertyFromResource(o, model, DATA_PROPERTY_ATTRIBUTE_ID))
        );
        if (extendedIterator.hasNext()) {
            return AttributeCreatorService.fromResource(extendedIterator.next(), model);
        } else {
            return new RecproAttribute();
        }
    }

    public List<RecproAttribute> getByRecproElementId(String elementId) {
        OntModel model = modelService.getModel();
        ResIterator instances = ModelingService.getResIterator(model, ATTRIBUTE);
        ExtendedIterator<Resource> extendedIterator = instances.filterKeep(
                o -> {
                    if (
                            o.hasProperty(ModelingService.getPropertyFromModel(model, DATA_PROPERTY_ATTRIBUTE_ID)) &&
                                    o.hasProperty(ModelingService.getPropertyFromModel(model, OBJECT_PROPERTY_ATTRIBUTE_IS_PART_OF_RECPRO_ELEMENT))
                    ) {
                        StmtIterator stmtIterator = o.listProperties(ModelingService.getPropertyFromModel(model, OBJECT_PROPERTY_ATTRIBUTE_IS_PART_OF_RECPRO_ELEMENT));
                        return stmtIterator.filterKeep(
                                statement -> ModelingService.getStringPropertyFromResource(statement.getResource(), model, DATA_PROPERTY_RECPRO_ELEMENT_ID).equals(elementId)
                        ).hasNext();
                    }
                    return false;
                }
        );
        return AttributeCreatorService.fromIterator(extendedIterator, model);
    }

    public List<RecproAttribute> getByActivityIds(List<String> activityIds) {
        OntModel model = modelService.getModel();
        ResIterator instances = ModelingService.getResIterator(model, ATTRIBUTE);
        ExtendedIterator<Resource> extendedIterator = instances.filterKeep(
                o -> {
                    if (
                            o.hasProperty(ModelingService.getPropertyFromModel(model, DATA_PROPERTY_ATTRIBUTE_ID)) &&
                                    o.hasProperty(ModelingService.getPropertyFromModel(model, OBJECT_PROPERTY_ATTRIBUTE_IS_PART_OF_RECPRO_ELEMENT))
                    ) {
                        StmtIterator stmtIterator = o.listProperties(ModelingService.getPropertyFromModel(model, OBJECT_PROPERTY_ATTRIBUTE_IS_PART_OF_RECPRO_ELEMENT));
                        return stmtIterator.filterKeep(
                                statement -> activityIds.contains(ModelingService.getStringPropertyFromResource(statement.getResource(), model, DATA_PROPERTY_RECPRO_ELEMENT_ID))
                        ).hasNext();
                    }
                    return false;
                }
        );
        return AttributeCreatorService.fromIterator(extendedIterator, model);
    }
}
