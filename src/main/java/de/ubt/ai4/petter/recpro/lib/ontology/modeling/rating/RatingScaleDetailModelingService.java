package de.ubt.ai4.petter.recpro.lib.ontology.modeling.rating;

import de.ubt.ai4.petter.recpro.lib.ontology.modeling.service.ModelingService;
import de.ubt.ai4.petter.recpro.lib.rating.rating.modeling.model.ScaleDetail;
import lombok.AllArgsConstructor;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.ObjectProperty;
import org.apache.jena.ontology.OntModel;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

import static de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyIds.*;

@AllArgsConstructor
@Service
public class RatingScaleDetailModelingService {

    public void createRatingScaleDetail(List<ScaleDetail> scaleDetails, Individual scaleIndividual, OntModel model) {
        scaleDetails.forEach(scaleDetail -> createRatingScaleDetail(scaleDetail, scaleIndividual, model));
    }

    public void createRatingScaleDetail(ScaleDetail scaleDetail, Individual scaleIndividual, OntModel model) {
        if (scaleDetail.getId().isEmpty()) {
            scaleDetail.setId(UUID.randomUUID().toString());
        }
        Individual scaleDetailIndividual = ModelingService.getIndividual(model, RATING_SCALE_DETAIL, scaleDetail.getId());

        this.createDataProperties(scaleDetailIndividual, model, scaleDetail);
        this.createObjectProperties(model, scaleDetailIndividual, scaleIndividual);
    }

    private void createDataProperties(Individual individual, OntModel model, ScaleDetail scaleDetail) {
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_RATING_SCALE_DETAIL_ID, individual, scaleDetail.getId());
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_RATING_SCALE_DETAIL_LABEL, individual, scaleDetail.getLabel());
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_RATING_SCALE_DETAIL_VALUE, individual, String.valueOf(scaleDetail.getValue()));
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_RATING_SCALE_DETAIL_POSITION, individual, String.valueOf(scaleDetail.getPosition()));
    }

    private void createObjectProperties(OntModel model, Individual scaleDetailIndividual, Individual scaleIndividual) {
        ObjectProperty scaleHasScaleDetail = ModelingService.getObjectPropertyFromModel(model, OBJECT_PROPERTY_RATING_SCALE_HAS_SCALE_DETAIL);

        ModelingService.addProperty(scaleIndividual, scaleHasScaleDetail, scaleDetailIndividual);
    }

}
