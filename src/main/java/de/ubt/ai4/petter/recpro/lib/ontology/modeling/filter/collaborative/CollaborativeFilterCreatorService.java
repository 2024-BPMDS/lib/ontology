package de.ubt.ai4.petter.recpro.lib.ontology.modeling.filter.collaborative;

import de.ubt.ai4.petter.recpro.lib.filter.basefilter.model.CollaborativeFilter;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.filter.util.FilterCreatorUtil;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.service.ModelingService;
import lombok.AllArgsConstructor;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.Resource;
import org.springframework.stereotype.Service;

import static de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyFilterIds.*;

@AllArgsConstructor
@Service
public class CollaborativeFilterCreatorService {
    private FilterCreatorUtil filterCreatorUtil;

    public CollaborativeFilter fromResource(Resource resource, OntModel model) {
        CollaborativeFilter result = new CollaborativeFilter();
        filterCreatorUtil.initialize(result, resource, model);

        result.setAllInputAttributes(Boolean.parseBoolean(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_FILTER_ALL_INPUT_ATTRIBUTES)));
        result.setAllInputElements(Boolean.parseBoolean(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_FILTER_ALL_INPUT_ELEMENTS)));
        result.setAllInputRatings(Boolean.parseBoolean(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_FILTER_ALL_INPUT_RATINGS)));
        result.setAllInputUsers(Boolean.parseBoolean(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_FILTER_ALL_INPUT_USERS)));

        result.setAttributes(filterCreatorUtil.setAttributes(result.isAllInputAttributes(), resource, model));
        result.setBpmElements(filterCreatorUtil.setElements(result.isAllInputElements(), resource, model));
        result.setRatings(filterCreatorUtil.setRatings(result.isAllInputRatings(), resource, model));
        result.setUsers(filterCreatorUtil.setUsers(result.isAllInputUsers(), resource, model));
        return result;
    }
}
