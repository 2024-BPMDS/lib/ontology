package de.ubt.ai4.petter.recpro.lib.ontology.modeling.filter.base;

import de.ubt.ai4.petter.recpro.lib.filter.basefilter.model.*;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.filter.collaborative.CollaborativeFilterModelingService;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.filter.contentBased.ContentBasedFilterModelingService;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.filter.knowledgeBased.KnowledgeBasedFilterModelingService;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.service.ModelService;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.service.ModelingService;
import lombok.AllArgsConstructor;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

import static de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyFilterIds.*;

@AllArgsConstructor
@Service
public class FilterModelingService {
    private ModelService modelService;
    private KnowledgeBasedFilterModelingService knowledgeBasedFilterModelingService;
    private BaseFilterModelingService baseFilterModelingService;
    private ContentBasedFilterModelingService contentBasedFilterModelingService;
    private CollaborativeFilterModelingService collaborativeFilterModelingService;
    private FilterCreatorService filterCreatorService;

    public Filter create(Filter filter) {
        if (filter.getId().isEmpty() || filter.getId().equals("-1")) {
            filter.setId(UUID.randomUUID().toString());
        }
        OntModel model = modelService.getModel();

        Individual filterIndividual = ModelingService.getIndividual(model, FILTER, filter.getId());
        filterIndividual.remove();

        switch (filter.getFilterType()) {
            case KNOWLEDGE_BASED -> filterIndividual = this.knowledgeBasedFilterModelingService.createFilterIndividual((KnowledgeBasedFilter) filter, model);
            case BASE -> filterIndividual = this.baseFilterModelingService.createFilterIndividual((BaseFilter) filter, model);
            case CONTENT_BASED -> filterIndividual = this.contentBasedFilterModelingService.createFilterIndividual((ContentBasedFilter) filter, model);
            case COLLABORATIVE -> filterIndividual = this.collaborativeFilterModelingService.createFilterIndividual((CollaborativeFilter) filter, model);
            default -> filterIndividual = this.createFilterIndividual(filter, model);
        }

        this.createDataProperties(model, filterIndividual, filter);

        modelService.writeModel(model);
        return filter;
    }

    private Individual createFilterIndividual(Filter filter, OntModel model) {
        Individual filterIndividual = ModelingService.getIndividual(model, FILTER, filter.getId());
        this.createDataProperties(model, filterIndividual, filter);
        return filterIndividual;
    }

    public void createDataProperties(OntModel model, Individual filterIndividual, Filter filter) {
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_FILTER_ID, filterIndividual, filter.getId());
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_FILTER_DESCRIPTION, filterIndividual, filter.getDescription());
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_FILTER_NAME, filterIndividual, filter.getName());
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_FILTER_URL, filterIndividual, filter.getFilterUrl());
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_FILTER_TYPE, filterIndividual, filter.getFilterType().name());
    }

    public List<Filter> getAll() {
        OntModel model = modelService.getModel();
        return filterCreatorService.fromIterator(ModelingService.getResIterator(model, FILTER), model);
    }

    public void removeFilter(String filterId) {
        OntModel model = modelService.getModel();
        Individual individual = ModelingService.getIndividual(model, FILTER, filterId);
        model.removeAll(individual, null, null);
        model.removeAll(null, null, individual);
        modelService.writeModel(model);
    }

    public Filter getById(String id) {
        OntModel model = modelService.getModel();
        ResIterator instances = ModelingService.getResIterator(model, FILTER);
        ExtendedIterator<Resource> extendedIterator = instances.filterKeep(
                o -> o.hasProperty(ModelingService.getPropertyFromModel(model, DATA_PROPERTY_FILTER_ID))
                        && id.equals(ModelingService.getStringPropertyFromResource(o, model, DATA_PROPERTY_FILTER_ID))
        );
        if (extendedIterator.hasNext()) {
            return filterCreatorService.fromResource(extendedIterator.next(), model);
        } else {
            return new Filter();
        }
    }

    public List<Filter> getByType(FilterType type) {
        OntModel model = modelService.getModel();

        return switch (type) {
            case BASE -> filterCreatorService.fromIterator(ModelingService.getResIterator(model, FILTER_BASE), model);
            case CONTENT_BASED -> filterCreatorService.fromIterator(ModelingService.getResIterator(model, FILTER_CONTENT_BASED), model);
            case KNOWLEDGE_BASED -> filterCreatorService.fromIterator(ModelingService.getResIterator(model, FILTER_KNOWLEDGE_BASED), model);
            case COLLABORATIVE -> filterCreatorService.fromIterator(ModelingService.getResIterator(model, FILTER_COLLABORATIVE), model);
            case HYBRID -> filterCreatorService.fromIterator(ModelingService.getResIterator(model, FILTER_HYBRID), model);
            default -> filterCreatorService.fromIterator(ModelingService.getResIterator(model, FILTER), model);
        };
    }
}
