package de.ubt.ai4.petter.recpro.lib.ontology.modeling.rating;

import de.ubt.ai4.petter.recpro.lib.ontology.modeling.service.ModelingService;
import de.ubt.ai4.petter.recpro.lib.rating.rating.modeling.model.Scale;
import lombok.AllArgsConstructor;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.*;
import org.springframework.stereotype.Service;

import static de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyBaseIds.*;
import static de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyIds.*;

@AllArgsConstructor
@Service
public class RatingScaleCreatorService {

    public static Scale fromResource(Resource resource, OntModel model) {
        Property property = model.getProperty(BASE_URL + OBJECT_PROPERTY_RATING_HAS_RATING_SCALE);
        StmtIterator iterator = model.listStatements(resource, property, (RDFNode) null);

        Scale scale = new Scale();
        while (iterator.hasNext()) {
            Statement statement = iterator.nextStatement();
            Resource scaleRes = statement.getObject().asResource();
            scale.setId(ModelingService.getStringPropertyFromResource(scaleRes, model, DATA_PROPERTY_RATING_SCALE_ID));
            scale.setDetails(RatingScaleDetailCreatorService.scaleDetailsFromResource(scaleRes, model));
        }
        return scale;
    }
}
