package de.ubt.ai4.petter.recpro.lib.ontology.modeling.bpm.modelingService;

import de.ubt.ai4.petter.recpro.lib.bpm.model.modeling.Activity;
import de.ubt.ai4.petter.recpro.lib.bpm.model.modeling.ElementType;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.bpm.creatorService.ActivityCreatorService;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.service.ModelService;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.service.ModelingService;
import lombok.AllArgsConstructor;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.ObjectProperty;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.springframework.stereotype.Service;

import java.util.List;

import static de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyIds.*;

@AllArgsConstructor
@Service
public class OntologyModelingBpmActivityService {

    private ModelService modelService;
    private OntologyModelingBpmElementService elementService;
    private OntologyModelingBpmRoleService roleService;

    public void createActivities(List<Activity> activities, OntModel model, Individual processModelIndividual) {
        activities.forEach(activity -> createActivity(activity, model, processModelIndividual));
    }

    public void createActivity(Activity activity, OntModel model, Individual processModelIndividual) {
        Individual activityIndividual = ModelingService.getIndividual(model, ACTIVITY, activity.getId());
        this.createDataProperties(model, activityIndividual, activity);
        this.createObjectProperties(model, activityIndividual, processModelIndividual);
        roleService.createRoles(activity.getRoles(), model, activityIndividual, true);
    }

    private void createDataProperties(OntModel model, Individual activityIndividual, Activity activity) {
        elementService.createRecproElementIndividual(activity, activityIndividual, model);
    }

    private void createObjectProperties(OntModel model, Individual activityIndividual, Individual processModelIndividual) {
        ObjectProperty processModelHasActivity = ModelingService.getObjectPropertyFromModel(model, OBJECT_PROPERTY_PROCESS_MODEL_HAS_ACTIVITY);
        ObjectProperty activityIsPartOfProcessModel = ModelingService.getObjectPropertyFromModel(model, OBJECT_PROPERTY_ACTIVITY_IS_PART_OF_PROCESS_MODEL);

        ModelingService.addProperty(processModelIndividual, processModelHasActivity, activityIndividual);
        ModelingService.addProperty(activityIndividual, activityIsPartOfProcessModel, processModelIndividual);
    }

    public Activity getById(String activityId) {
        OntModel model = modelService.getModel();
        ResIterator individuals = ModelingService.getResIterator(model, ACTIVITY);
        ExtendedIterator<Resource> extendedIterator = individuals.filterKeep(
                resource -> resource.hasProperty(ModelingService.getPropertyFromModel(model, DATA_PROPERTY_RECPRO_ELEMENT_ID))
                && activityId.equals(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_RECPRO_ELEMENT_ID))
                && ElementType.PROCESS_ACTIVITY.equals(ElementType.findType(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_RECPRO_ELEMENT_TYPE)))
        );
        if (extendedIterator.hasNext()) {
            return ActivityCreatorService.activityFromResource(extendedIterator.next(), model);
        } else {
            return new Activity();
        }
    }

    public List<Activity> getAll() {
        OntModel model = modelService.getModel();
        ResIterator individuals = ModelingService.getResIterator(model, ACTIVITY);
        return ActivityCreatorService.activitiesFromResIterator(individuals, model);
    }

}
