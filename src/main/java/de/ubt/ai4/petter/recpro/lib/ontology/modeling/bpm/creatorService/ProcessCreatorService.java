package de.ubt.ai4.petter.recpro.lib.ontology.modeling.bpm.creatorService;

import de.ubt.ai4.petter.recpro.lib.bpm.model.modeling.Process;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.service.ModelingService;
import de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyIds;
import lombok.AllArgsConstructor;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Service
public class ProcessCreatorService {
    public static List<Process> processFromResIterator(ResIterator instances, OntModel model) {
        List<Process> result = new ArrayList<>();
        while (instances.hasNext()) {
            Resource instance = instances.nextResource();
            result.add(processFromResource(instance, model));
        }
        return result;
    }

    public static List<Process> processFromResIterator(ExtendedIterator<Resource> instances, OntModel model) {
        List<Process> result = new ArrayList<>();
        while (instances.hasNext()) {
            Resource instance = instances.next();
            result.add(processFromResource(instance, model));
        }
        return result;
    }

    public static Process processFromResource(Resource resource, OntModel model) {
        Process result = new Process();
        ElementCreatorService.initialize(result, resource, model);
        result.setVersion(ModelingService.getStringPropertyFromResource(resource, model, OntologyIds.DATA_PROPERTY_PROCESS_VERSION));
        result.setKey(ModelingService.getStringPropertyFromResource(resource, model, OntologyIds.DATA_PROPERTY_PROCESS_KEY));
        result.setLatest(Boolean.parseBoolean(ModelingService.getStringPropertyFromResource(resource, model, OntologyIds.DATA_PROPERTY_PROCESS_IS_LATEST)));
        result.setProcessModel(ProcessModelCreatorService.processModelFromStatement(ModelingService.getPropertyFromResource(resource, model, OntologyIds.OBJECT_PROPERTY_PROCESS_HAS_PROCESS_MODEL), model));
        return result;
    }
}
