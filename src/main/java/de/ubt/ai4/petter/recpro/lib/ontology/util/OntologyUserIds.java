package de.ubt.ai4.petter.recpro.lib.ontology.util;

public final class OntologyUserIds {
    public static final String USER = "User";

    public static final String DATA_PROPERTY_USER_HAS_DESCRIPTION = "userHasDescription";
    public static final String DATA_PROPERTY_USER_HAS_ID = "userHasId";
    public static final String DATA_PROPERTY_USER_HAS_LAST_NAME = "userHasLastName";
    public static final String DATA_PROPERTY_USER_HAS_FIRST_NAME = "userHasFirstName";

    public static final String OBJECT_PROPERTY_USER_HAS_ROLE = "userHasRole";
}
