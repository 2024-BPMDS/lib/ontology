package de.ubt.ai4.petter.recpro.lib.ontology.modeling.filter.knowledgeBased;

import de.ubt.ai4.petter.recpro.lib.filter.basefilter.model.KnowledgeBasedFilter;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.service.ModelingService;
import lombok.AllArgsConstructor;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.ObjectProperty;
import org.apache.jena.ontology.OntModel;
import org.springframework.stereotype.Service;

import static de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyAttributeIds.ATTRIBUTE;
import static de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyFilterIds.*;
import static de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyIds.*;

@AllArgsConstructor
@Service
public class KnowledgeBasedFilterModelingService {

    public Individual createFilterIndividual(KnowledgeBasedFilter filter, OntModel model) {
        Individual filterIndividual = ModelingService.getIndividual(model, FILTER_KNOWLEDGE_BASED, filter.getId());
        this.createObjectProperties(model, filterIndividual, filter);
        this.createDataProperties(model, filterIndividual, filter);
        return filterIndividual;
    }

    private void createDataProperties(OntModel model, Individual filterIndividual, KnowledgeBasedFilter filter) {
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_FILTER_ALL_INPUT_ATTRIBUTES, filterIndividual, String.valueOf(filter.isAllInputAttributes()));
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_FILTER_ALL_INPUT_ELEMENTS, filterIndividual, String.valueOf(filter.isAllInputElements()));
    }

    private void createObjectProperties(OntModel model, Individual filterIndividual, KnowledgeBasedFilter filter) {
        ObjectProperty filterUsesAttribute = ModelingService.getObjectPropertyFromModel(model, OBJECT_PROPERTY_FILTER_USES_ATTRIBUTE);
        ObjectProperty filterUsesRecproElement = ModelingService.getObjectPropertyFromModel(model, OBJECT_PROPERTY_FILTER_USES_BPM_ELEMENT);

        filterIndividual.removeAll(filterUsesAttribute);
        filterIndividual.removeAll(filterUsesRecproElement);

        if (!filter.isAllInputAttributes()) {
            filter.getAttributes().forEach(attribute -> {
                Individual individual = ModelingService.getIndividual(model, ATTRIBUTE, attribute.getId());
                ModelingService.addProperty(filterIndividual, filterUsesAttribute, individual);
            });
        }

        if (!filter.isAllInputElements()) {
            filter.getBpmElements().forEach(recproElement -> {
                Individual individual = ModelingService.getIndividual(model, RECPRO_ELEMENT, recproElement.getId());
                ModelingService.addProperty(filterIndividual, filterUsesRecproElement, individual);
            });
        }
    }
}
