package de.ubt.ai4.petter.recpro.lib.ontology.modeling.filter.base;

import de.ubt.ai4.petter.recpro.lib.filter.basefilter.model.BaseFilter;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.service.ModelingService;
import lombok.AllArgsConstructor;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntModel;
import org.springframework.stereotype.Service;

import static de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyFilterIds.*;

@AllArgsConstructor
@Service
public class BaseFilterModelingService {

    public Individual createFilterIndividual(BaseFilter filter, OntModel model) {
        Individual filterIndividual = ModelingService.getIndividual(model, FILTER_BASE, filter.getId());
        this.createDataProperties(model, filterIndividual, filter);
        return filterIndividual;
    }

    private void createDataProperties(OntModel model, Individual filterIndividual, BaseFilter filter) {
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_FILTER_TASKLIST_ASCENDING, filterIndividual, String.valueOf(filter.isAscending()));
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_FILTER_TASKLIST_ORDER, filterIndividual, filter.getTasklistOrder().name());
    }
}
